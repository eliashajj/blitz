function WPATH(s) {
    var index = s.lastIndexOf("/");
    var path = -1 === index ? "com.capnajax.vectorimage/" + s : s.substring(0, index) + "/com.capnajax.vectorimage/" + s.substring(index + 1);
    return path;
}

function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function init(obj) {
        $.widget.applyProperties(_.omit(obj, [ "svg", "style" ]));
        draw(obj.svg, obj.style, obj.width, obj.height, obj.backgroundColor);
    }
    function sourceText(source) {
        var result;
        if (_.isObject(source)) {
            if (source.apiName != Ti.Filesystem.File) throw new Error("com.capnajax.vectorimage::widget::sourceText invalid object, apiName " + source.apiName);
            result = source.read().text;
            Ti.API.debug("com.capnajax.vectorimage::sourceText:: source file read as :\n" + result);
        } else {
            if (!_.isString(source)) throw new Error("com.capnajax.vectorimage::widget::sourceText invalid param of type " + typeof source);
            if (/[\<\{]/.test(source)) {
                Ti.API.debug("com.capnajax.vectorimage::sourceText:: raw source:\n" + source);
                result = source;
            } else {
                /^\//.test(source) || (source = Ti.Filesystem.resourcesDirectory + "/" + source);
                Ti.API.debug('com.capnajax.vectorimage::sourceText:: file = "' + source + '"');
                result = Ti.Filesystem.getFile(source).read().text;
            }
        }
        Ti.API.debug('com.capnajax.vectorimage::sourceText:: returning:\n = "' + result + '"');
        return result;
    }
    function draw(svg, style, width, height, backgroundColor, callback) {
        var svgString = svg && sourceText(svg);
        var styleString = style && sourceText(style);
        Ti.API.debug("com.capnajax.vectorimage::widget::draw svgString == " + svgString);
        Ti.API.debug("com.capnajax.vectorimage::widget::draw styleString == " + styleString);
        $.widget.removeAllChildren();
        $.widget.add(drawWebView(svgString, styleString, width, height, backgroundColor, callback));
    }
    function drawWebView(svg, style, width, height, backgroundColor, callback) {
        var svgStyled = svg;
        if (style) {
            Ti.API.debug("styled, /(<svg[^>]*>)/.test(svg) == " + /(\<svg[^\>]*\>)/.test(svg));
            var styletype = /^\</.test(style) ? "application/xml" : "text/css";
            svgStyled = svg.replace(/(\<svg[^\>]*\>)/, '$1<defs><style type="' + styletype + '"><![CDATA[' + style + "]]></style></defs>");
        }
        var html = '<html><head><title></title><head><body style="margin:0;padding:0">' + svgStyled + "</body></html>";
        Ti.API.debug("com.capnajax.vectorimage::widget::drawWebView html == " + html);
        var view = Ti.UI.createWebView({
            width: width,
            height: height,
            html: html,
            backgroundColor: backgroundColor,
            showScrollbars: false,
            touchEnabled: false,
            overScrollMode: null,
            opacity: 1
        });
        callback && view.addEventListener("load", callback);
        return view;
    }
    new (require("alloy/widget"))("com.capnajax.vectorimage");
    this.__widgetId = "com.capnajax.vectorimage";
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "widget";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.widget = Ti.UI.createView({
        id: "widget"
    });
    $.__views.widget && $.addTopLevelView($.__views.widget);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    Ti.API.debug("com.capnajax.vectorimage::widget:: args = " + JSON.stringify(args));
    init(args);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;