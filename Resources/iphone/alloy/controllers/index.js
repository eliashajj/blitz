function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.index = Ti.UI.createWindow({
        backgroundColor: "#ed552a",
        font: {
            fontFamily: "Roboto-Light"
        },
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    $.__views.logo = Ti.UI.createView({
        width: "24%",
        height: "24%",
        top: "15%",
        id: "logo"
    });
    $.__views.index.add($.__views.logo);
    $.__views.__alloyId0 = Ti.UI.createImageView({
        image: "/svg/logo.png",
        id: "__alloyId0"
    });
    $.__views.logo.add($.__views.__alloyId0);
    $.__views.fbLogin = Ti.UI.createView({
        width: "80%",
        height: "35dp",
        backgroundColor: "#d65230",
        top: "48%",
        id: "fbLogin"
    });
    $.__views.index.add($.__views.fbLogin);
    $.__views.__alloyId1 = Ti.UI.createView({
        width: Ti.UI.SIZE,
        height: "26dp",
        layout: "horizontal",
        top: "4dp",
        id: "__alloyId1"
    });
    $.__views.fbLogin.add($.__views.__alloyId1);
    $.__views.__alloyId2 = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#FFF",
        font: {
            fontSize: 13,
            fontFamily: "Roboto-Light"
        },
        text: "LOGIN WITH",
        id: "__alloyId2"
    });
    $.__views.__alloyId1.add($.__views.__alloyId2);
    $.__views.__alloyId3 = Ti.UI.createImageView({
        width: "26dp",
        height: "26dp",
        left: "20dp",
        image: "/images/facebook.png",
        id: "__alloyId3"
    });
    $.__views.__alloyId1.add($.__views.__alloyId3);
    $.__views.gplusLogin = Ti.UI.createView({
        width: "80%",
        height: "35dp",
        backgroundColor: "#d65230",
        top: "58%",
        id: "gplusLogin"
    });
    $.__views.index.add($.__views.gplusLogin);
    $.__views.__alloyId4 = Ti.UI.createView({
        width: Ti.UI.SIZE,
        height: "26dp",
        layout: "horizontal",
        top: "4dp",
        id: "__alloyId4"
    });
    $.__views.gplusLogin.add($.__views.__alloyId4);
    $.__views.__alloyId5 = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#FFF",
        font: {
            fontSize: 13,
            fontFamily: "Roboto-Light"
        },
        text: "LOGIN WITH",
        id: "__alloyId5"
    });
    $.__views.__alloyId4.add($.__views.__alloyId5);
    $.__views.__alloyId6 = Ti.UI.createImageView({
        width: "26dp",
        height: "26dp",
        left: "20dp",
        image: "/images/gplus.png",
        id: "__alloyId6"
    });
    $.__views.__alloyId4.add($.__views.__alloyId6);
    $.__views.__alloyId7 = Ti.UI.createView({
        width: Ti.UI.SIZE,
        height: "26dp",
        layout: "horizontal",
        top: "4dp",
        id: "__alloyId7"
    });
    $.__views.index.add($.__views.__alloyId7);
    $.__views.facebookInfo = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#FFF",
        font: {
            fontSize: 13,
            fontFamily: "Roboto-Light"
        },
        id: "facebookInfo"
    });
    $.__views.__alloyId7.add($.__views.facebookInfo);
    $.__views.fbImage = Ti.UI.createImageView({
        id: "fbImage"
    });
    $.__views.index.add($.__views.fbImage);
    exports.destroy = function() {};
    _.extend($, $.__views);
    if ("" != Alloy.Globals.LoginId && "undefined" != Alloy.Globals.LoginId && null != Alloy.Globals.LoginId) {
        Ti.API.info("User ID Exists");
        $.index.close();
        Alloy.createController("profile").getView().open();
    }
    Ti.API.info(Alloy.Globals.LoginId);
    var google = require("GoogleService").GoogleService;
    $.gplusLogin.addEventListener("click", function() {
        google.setClientId(Alloy.CFG.googleClientIdIos);
        google.setScopes([ "profile", "email" ]);
        google.signin({
            success: function(event) {
                Ti.API.info(event);
                event.accessToken ? doServerPost({
                    provider: "google",
                    code: event.accessToken
                }) : handleBadPost();
            },
            error: function(event) {
                Ti.API.info("ERROR! " + JSON.stringify(event));
                handleBadPost();
            }
        });
    });
    Alloy.Globals.Facebook.addEventListener("login", function(e) {
        if (e.success) {
            var fbUser = JSON.parse(e.data);
            Ti.App.Properties.setString("logId", fbUser.id);
            Ti.App.Properties.setString("logName", fbUser.name);
            Ti.App.Properties.setString("logMedium", "fb");
            {
                Alloy.Globals.Facebook.getAccessToken();
            }
            Alloy.Globals.Facebook.requestWithGraphPath("me/picture", {
                redirect: "false",
                width: "230",
                hight: "230"
            }, "GET", function(evt) {
                if (evt.success) {
                    var fbResult = JSON.parse(evt.result);
                    var fbData = fbResult.data.url;
                    Ti.App.Properties.setString("profilePic", fbData);
                    $.index.close();
                    Alloy.createController("profile").getView().open();
                } else Ti.API.info(evt.error ? "Error: " + evt.error : "Unknown response");
            });
        } else e.cancelled || Ti.API.debug("Failed authorization due to: " + e.error);
    });
    $.fbLogin.addEventListener("click", function() {
        Ti.API.info("Clicked");
        Alloy.Globals.Facebook.initialize(1e3);
        Alloy.Globals.Facebook.authorize();
    });
    $.index.open();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;