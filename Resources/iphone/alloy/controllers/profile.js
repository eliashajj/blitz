function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "profile";
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.profile = Ti.UI.createWindow({
        backgroundColor: "#ed552a",
        font: {
            fontFamily: "Roboto-Light"
        },
        width: "100%",
        height: "100%",
        id: "profile",
        modal: "true"
    });
    $.__views.profile && $.addTopLevelView($.__views.profile);
    $.__views.sideMenu = Ti.UI.createView({
        width: "100%",
        height: Ti.Platform.displayCaps.PlatformWHeight,
        zIndex: 10,
        id: "sideMenu"
    });
    $.__views.profile.add($.__views.sideMenu);
    $.__views.viewInner = Ti.UI.createView({
        width: "100%",
        height: "100%",
        backgroundColor: "#e46035",
        opacity: "0.9",
        id: "viewInner"
    });
    $.__views.sideMenu.add($.__views.viewInner);
    $.__views.wrapper = Ti.UI.createView({
        zIndex: 1,
        layout: "vertical",
        id: "wrapper"
    });
    $.__views.profile.add($.__views.wrapper);
    $.__views.__alloyId8 = Ti.UI.createView({
        width: "100%",
        height: "6%",
        backgroundColor: "#ed552a",
        id: "__alloyId8"
    });
    $.__views.wrapper.add($.__views.__alloyId8);
    $.__views.__alloyId9 = Ti.UI.createView({
        width: Ti.UI.FILL,
        height: "auto",
        layout: "horizontal",
        top: "12%",
        id: "__alloyId9"
    });
    $.__views.__alloyId8.add($.__views.__alloyId9);
    $.__views.drawer = Ti.UI.createImageView({
        left: "5dp",
        id: "drawer",
        image: "/images/drawer.png"
    });
    $.__views.__alloyId9.add($.__views.drawer);
    $.__views.logo = Ti.UI.createImageView({
        left: "40%",
        id: "logo",
        image: "/images/blitz.png"
    });
    $.__views.__alloyId9.add($.__views.logo);
    $.__views.__alloyId10 = Ti.UI.createView({
        width: "100%",
        height: "0.1%",
        backgroundColor: "#8a3c1e",
        id: "__alloyId10"
    });
    $.__views.wrapper.add($.__views.__alloyId10);
    $.__views.profileBox = Ti.UI.createView({
        width: "100%",
        height: "25%",
        backgroundColor: "#ed552a",
        id: "profileBox"
    });
    $.__views.wrapper.add($.__views.profileBox);
    $.__views.profileContent = Ti.UI.createView({
        width: Ti.UI.FILL,
        height: Ti.UI.SIZE,
        top: "8.2%",
        left: "2%",
        right: "2%",
        borderWidth: "1",
        id: "profileContent"
    });
    $.__views.profileBox.add($.__views.profileContent);
    $.__views.profilePic = Ti.UI.createImageView({
        width: "35%",
        left: "0",
        top: "0",
        id: "profilePic"
    });
    $.__views.profileContent.add($.__views.profilePic);
    $.__views.profileInfo = Ti.UI.createView({
        width: "61%",
        height: Ti.UI.FILL,
        left: "39%",
        top: "0",
        textAlign: "left",
        layout: "vertical",
        borderWidth: "1",
        borderColor: "#FF0000",
        id: "profileInfo"
    });
    $.__views.profileContent.add($.__views.profileInfo);
    $.__views.userName = Ti.UI.createLabel({
        font: {
            fontFamily: "Roboto-Light"
        },
        color: "#FFFFFF",
        textAlign: "left",
        left: "0",
        text: "",
        id: "userName"
    });
    $.__views.profileInfo.add($.__views.userName);
    $.__views.userInfo = Ti.UI.createLabel({
        font: {
            fontFamily: "Roboto-Light"
        },
        color: "#FFFFFF",
        textAlign: "left",
        left: "0",
        height: Ti.UI.SIZE,
        borderWidth: "1",
        text: "Acctount balance",
        id: "userInfo"
    });
    $.__views.profileInfo.add($.__views.userInfo);
    $.__views.label2 = Ti.UI.createLabel({
        font: {
            fontFamily: "Roboto-Light"
        },
        color: "#FFFFFF",
        textAlign: "left",
        left: "0",
        height: Ti.UI.SIZE,
        borderWidth: "1",
        text: "3 Accomplished dares",
        id: "label2"
    });
    $.__views.profileInfo.add($.__views.label2);
    $.__views.label3 = Ti.UI.createLabel({
        font: {
            fontFamily: "Roboto-Light"
        },
        color: "#FFFFFF",
        textAlign: "left",
        left: "0",
        height: Ti.UI.SIZE,
        borderWidth: "1",
        text: "1 Uploaded dare",
        id: "label3"
    });
    $.__views.profileInfo.add($.__views.label3);
    $.__views.followers = Ti.UI.createView({
        width: Ti.UI.FILL,
        height: "20dp",
        borderWidth: "1",
        borderColor: "#FF00FF",
        id: "followers"
    });
    $.__views.profileInfo.add($.__views.followers);
    $.__views.follow = Ti.UI.createLabel({
        font: {
            fontFamily: "Roboto-Regular",
            fontSize: "11dp"
        },
        color: "#FFFFFF",
        textAlign: "center",
        left: "0",
        width: "48%",
        height: "20dp",
        backgroundColor: "#4C4C4C",
        text: "+ Follow",
        id: "follow"
    });
    $.__views.followers.add($.__views.follow);
    $.__views.addDare = Ti.UI.createLabel({
        font: {
            fontFamily: "Roboto-Regular",
            fontSize: "11dp"
        },
        color: "#FFFFFF",
        textAlign: "center",
        left: "50%",
        width: "48%",
        height: "20dp",
        backgroundColor: "#8a3c1e",
        text: "+ Add a dare",
        id: "addDare"
    });
    $.__views.followers.add($.__views.addDare);
    $.__views.tabs = Ti.UI.createView({
        width: Ti.UI.FILL,
        height: "35dp",
        id: "tabs"
    });
    $.__views.wrapper.add($.__views.tabs);
    $.__views.tab1 = Ti.UI.createView({
        width: "33%",
        height: "35dp",
        left: "0",
        backgroundColor: "#ed552a",
        layout: "vertical",
        textAlign: "center",
        id: "tab1"
    });
    $.__views.tabs.add($.__views.tab1);
    $.__views.__alloyId11 = Ti.UI.createLabel({
        font: {
            fontFamily: "Roboto-Light",
            fontSize: "16dp"
        },
        color: "#FFFFFF",
        textAlign: "center",
        left: "0",
        width: Ti.UI.FILL,
        text: "100",
        id: "__alloyId11"
    });
    $.__views.tab1.add($.__views.__alloyId11);
    $.__views.__alloyId12 = Ti.UI.createLabel({
        font: {
            fontFamily: "Roboto-Light",
            fontSize: "11dp"
        },
        color: "#FFFFFF",
        textAlign: "center",
        left: "0",
        width: Ti.UI.FILL,
        text: "followers",
        id: "__alloyId12"
    });
    $.__views.tab1.add($.__views.__alloyId12);
    $.__views.tab2 = Ti.UI.createView({
        width: "33%",
        height: "35dp",
        left: "33%",
        backgroundColor: "#8a3c1e",
        layout: "vertical",
        textAlign: "center",
        id: "tab2"
    });
    $.__views.tabs.add($.__views.tab2);
    $.__views.__alloyId13 = Ti.UI.createLabel({
        font: {
            fontFamily: "Roboto-Light",
            fontSize: "16dp"
        },
        color: "#FFFFFF",
        textAlign: "center",
        left: "0",
        width: Ti.UI.FILL,
        text: "12",
        id: "__alloyId13"
    });
    $.__views.tab2.add($.__views.__alloyId13);
    $.__views.__alloyId14 = Ti.UI.createLabel({
        font: {
            fontFamily: "Roboto-Light",
            fontSize: "11dp"
        },
        color: "#FFFFFF",
        textAlign: "center",
        left: "0",
        width: Ti.UI.FILL,
        text: "posts",
        id: "__alloyId14"
    });
    $.__views.tab2.add($.__views.__alloyId14);
    $.__views.tab3 = Ti.UI.createView({
        width: "34%",
        height: "35dp",
        left: "66%",
        backgroundColor: "#d05a31",
        layout: "vertical",
        textAlign: "center",
        id: "tab3"
    });
    $.__views.tabs.add($.__views.tab3);
    $.__views.__alloyId15 = Ti.UI.createLabel({
        font: {
            fontFamily: "Roboto-Light",
            fontSize: "16dp"
        },
        color: "#FFFFFF",
        textAlign: "center",
        left: "0",
        width: Ti.UI.FILL,
        text: "130",
        id: "__alloyId15"
    });
    $.__views.tab3.add($.__views.__alloyId15);
    $.__views.__alloyId16 = Ti.UI.createLabel({
        font: {
            fontFamily: "Roboto-Light",
            fontSize: "11dp"
        },
        color: "#FFFFFF",
        textAlign: "center",
        left: "0",
        width: Ti.UI.FILL,
        text: "following",
        id: "__alloyId16"
    });
    $.__views.tab3.add($.__views.__alloyId16);
    $.__views.dareList = Ti.UI.createView({
        width: Ti.UI.Fill,
        height: Ti.UI.Fill,
        backgroundColor: "#FFFFFF",
        id: "dareList"
    });
    $.__views.wrapper.add($.__views.dareList);
    $.__views.dList = Ti.UI.createImageView({
        top: "0",
        id: "dList",
        image: "/images/dareList.png"
    });
    $.__views.dareList.add($.__views.dList);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    $.profile.open();
    $.profilePic.image = Ti.App.Properties.getString("profilePic");
    $.userName.text = Ti.App.properties.getString("logName");
    var windowWidth = Ti.Platform.displayCaps.platformWidth;
    Ti.Platform.displayCaps.platformHeight;
    $.sideMenu.left = -windowWidth;
    $.drawer.addEventListener("click", function() {
        Ti.API.info("drwawer clicked");
        $.sideMenu.animate({
            left: "-60dp",
            duration: 500
        });
    });
    $.sideMenu.addEventListener("swipe", function(e) {
        "left" == e.direction && $.sideMenu.animate({
            left: -windowWidth,
            diration: 500
        });
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;