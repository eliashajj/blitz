function GoogleService(args) {
    self = this;
    clientId = args.clientId;
    clientSecret = args.clientSecret;
    redirectUri = args.redirectUri;
    devKey = args.devKey;
    Ti.API.debug("GoogleService object initialized");
}

function showAuthorizeUI(pUrl) {
    window = Ti.UI.createWindow({
        modal: true,
        fullscreen: true,
        width: "100%"
    });
    closeLabel = Ti.UI.createLabel({
        textAlign: "right",
        font: {
            fontWeight: "bold",
            fontSize: "12pt"
        },
        text: "(X)",
        top: 5,
        right: 12,
        height: 14
    });
    webView = Ti.UI.createWebView({
        top: 0,
        width: "100%",
        url: pUrl,
        autoDetect: [ Ti.UI.AUTODETECT_NONE ]
    });
    webView.addEventListener("beforeload", function(e) {
        Ti.API.info("Preload: " + e.url + " : " + e.url.indexOf(redirectUri));
        if (0 === e.url.indexOf("http://localhost/?")) {
            webView.stopLoading();
            authorizeUICallback(e);
        } else -1 !== e.url.indexOf("https://accounts.google.com/Logout") && authorizeUICallback(e);
    });
    webView.addEventListener("load", function(e) {
        Ti.API.info("Load: " + e.url + " : " + e.url.indexOf(redirectUri));
        -1 !== e.url.indexOf("approval") && authorizeUICallback(e);
    });
    closeLabel.addEventListener("click", destroyAuthorizeUI);
    window.add(closeLabel);
    window.add(webView);
    window.open();
}

function destroyAuthorizeUI() {
    Ti.API.debug("destroyAuthorizeUI");
    if (null == window) return;
    try {
        webView.removeEventListener("load", authorizeUICallback);
        window.remove(webView);
        webView = null;
        window.hide();
        window.close();
    } catch (ex) {
        Ti.API.error("Cannot destroy the authorize UI. Ignoring.");
    }
}

function getTokens(tempToken, callback) {
    var args = {
        call: "token",
        method: "POST",
        params: {
            client_id: clientId,
            client_secret: clientSecret,
            redirect_uri: redirectUri,
            grant_type: "authorization_code",
            code: tempToken
        }
    };
    self.callMethod(args, function(e) {
        Ti.API.info(e);
        e.success ? callback(e.data) : Ti.API.error("Error getting tokens");
    }, true);
}

function authorizeUICallback(e) {
    Ti.API.info("authorizeUILoaded " + e.type);
    var status;
    var tokenReturned = false;
    if (0 === e.url.indexOf("http://localhost/?")) {
        Ti.API.info(e.url);
        var token = e.url.split("=")[1];
        tokenReturned = true;
        Ti.API.info("Temp Token: " + token);
    } else if (-1 !== e.url.indexOf("approval")) {
        Ti.API.info(e.url);
        var title = webView.evalJS("document.title");
        var token = title.split("=")[1];
        tokenReturned = true;
        Ti.API.info("Temp Token: " + token);
    } else if (0 === e.url.indexOf("https://accounts.google.com/Logout")) {
        Ti.App.fireEvent("app:google_logout", {});
        destroyAuthorizeUI();
        Ti.App.Properties.setString("GG_ACCESS_TOKEN", null);
        Ti.App.Properties.setString("GG_REFRESH_TOKEN", null);
        success_callback(false);
    }
    if (tokenReturned) {
        if ("access_denied" === token) {
            Ti.App.fireEvent("app:google_access_denied", {});
            status = false;
            Ti.App.Properties.setString("GG_ACCESS_TOKEN", null);
            Ti.App.Properties.setString("GG_REFRESH_TOKEN", null);
            void 0 != success_callback && success_callback(status);
        } else getTokens(token, function(e) {
            var res = JSON.parse(e);
            Ti.API.debug(res);
            Ti.API.debug("Setting ACCESS_TOKEN: " + res.access_token);
            Ti.API.debug("Setting REFRESH_TOKEN: " + res.refresh_token);
            ACCESS_TOKEN = res.access_token;
            REFRESH_TOKEN = res.refresh_token;
            status = true;
            Ti.App.Properties.setString("GG_ACCESS_TOKEN", ACCESS_TOKEN);
            Ti.App.Properties.setString("GG_REFRESH_TOKEN", REFRESH_TOKEN);
            Ti.App.fireEvent("app:google_token", {
                access_token: ACCESS_TOKEN,
                refresh_token: REFRESH_TOKEN
            });
            void 0 != success_callback && success_callback(status);
        });
        destroyAuthorizeUI();
    }
}

var clientId;

var clientSecret;

var redirectUri;

var responseType = "code";

var accessType = "offline";

var scope = "https://www.googleapis.com/auth/userinfo.profile";

var state;

var devKey;

var ACCESS_TOKEN = null;

var REFRESH_TOKEN = null;

var xhr = null;

var OAUTH_URL = "https://accounts.google.com/o/oauth2/";

var API_URL = "https://www.googleapis.com/oauth2/v1/";

var success_callback = null;

var window = null;

var inSimulator = -1 !== Ti.Platform.model.indexOf("Simulator") || "x86_64" == Ti.Platform.model || "google_sdk" == Ti.Platform.model;

var self;

GoogleService.prototype.accessToken = function() {
    return Ti.App.Properties.getString("GG_ACCESS_TOKEN");
};

GoogleService.prototype.refreshToken = function(callback) {
    if (!REFRESH_TOKEN) return null;
    var args = {
        call: "token",
        method: "POST",
        params: {
            client_id: clientId,
            client_secret: clientSecret,
            refresh_token: REFRESH_TOKEN,
            grant_type: "refresh_token"
        }
    };
    self.callMethod(args, function(e) {
        Ti.API.info("[refreshToken] " + e);
        if (e.success) {
            Ti.API.info("[refreshToken] " + JSON.parse(e.data).access_token);
            ACCESS_TOKEN = JSON.parse(e.data).access_token;
            Ti.App.Properties.setString("GG_ACCESS_TOKEN", ACCESS_TOKEN);
            callback({
                success: true,
                token: ACCESS_TOKEN
            });
        } else {
            Ti.API.error("Error getting tokens");
            callback({
                success: false,
                token: null
            });
        }
    }, true);
};

GoogleService.prototype.login = function(authSuccess_callback) {
    ACCESS_TOKEN = Ti.App.Properties.getString("GG_ACCESS_TOKEN");
    REFRESH_TOKEN = Ti.App.Properties.getString("GG_REFRESH_TOKEN");
    Ti.API.info("GoogleService login " + ACCESS_TOKEN);
    if (null !== ACCESS_TOKEN) {
        ACCESS_TOKEN = Ti.App.Properties.getString("GG_ACCESS_TOKEN");
        success_callback = authSuccess_callback;
        authSuccess_callback(true);
        return;
    }
    void 0 !== authSuccess_callback && (success_callback = authSuccess_callback);
    var url = String.format(OAUTH_URL + "auth?client_id=%s&redirect_uri=%s&scope=%s&response_type=%s&access_type=%s", clientId, redirectUri, scope, responseType, accessType);
    showAuthorizeUI(url);
    return;
};

GoogleService.prototype.callMethod = function(args, callback, getToken) {
    var params = args.params;
    var method = args.method;
    var call = args.call;
    var paramsString = "";
    try {
        xhr || (xhr = Titanium.Network.createHTTPClient());
        if (params && "GET" === method) {
            for (var a in params) paramsString += "&" + Titanium.Network.encodeURIComponent(a) + "=" + Titanium.Network.encodeURIComponent(params[a]);
            var url = API_URL + call + "?access_token=" + ACCESS_TOKEN + "&alt=json&v=2&key=" + devKey + paramsString;
            Ti.API.info(url);
            xhr.open("GET", url);
        } else "POST" === method && (getToken ? xhr.open("POST", OAUTH_URL + call) : xhr.open("POST", API_URL + call + "?alt=json&v=2&access_token=" + ACCESS_TOKEN + "&key=" + devKey));
        xhr.onerror = function(e) {
            Ti.API.error("GoogleService ERROR " + e.error);
            Ti.API.error("GoogleService ERROR " + e);
            callback && callback({
                error: e,
                success: false
            });
        };
        xhr.onload = function() {
            callback && callback({
                error: null,
                success: true,
                data: xhr.responseText
            });
        };
        "POST" === method ? xhr.send(params) : xhr.send();
    } catch (err) {
        Titanium.UI.createAlertDialog({
            title: "Error",
            message: String(err),
            buttonNames: [ "OK" ]
        }).show();
    }
};

exports.GoogleService = GoogleService;