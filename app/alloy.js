Alloy.Globals.Facebook=require('facebook');
Alloy.Globals.Facebook.permissions=['public_profile', 'email'];
Alloy.Globals.LoginId=Ti.App.Properties.getString("logId");
Alloy.Globals.loginName=Ti.App.Properties.getString("logName");

Alloy.CFG.googleClientIdIos="775891618623-rh63v50cq54d1boqk4arv8uo233de8l7.apps.googleusercontent.com";

var getRemoteFile=function(file, remote) {
    var f=Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, file);
    if (!f.exists()) {
        var c=Titanium.Network.createHTTPClient();
        c.onload=function() {
            f.write(this.responseData);
        };
        c.open('GET', remote, true);
        c.send();
    }
    Ti.API.info(f.nativePath);
    return f.nativePath;
};