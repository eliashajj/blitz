if(Alloy.Globals.LoginId!="" && Alloy.Globals.LoginId!='undefined' && Alloy.Globals.LoginId!=null){
	Ti.API.info("User ID Exists");
	$.index.close();
	Alloy.createController('profile').getView().open();
}

Ti.API.info(Alloy.Globals.LoginId);

var google=require('GoogleService').GoogleService;;

$.gplusLogin.addEventListener('click',function(){
	google.setClientId(Alloy.CFG.googleClientIdIos);
	google.setScopes(["profile", "email"]);
	google.signin({
	  success: function(event) {
	  	Ti.API.info(event);
	    if (event.accessToken) {
	      doServerPost({provider: 'google', code: event.accessToken});
	    } else {
	      handleBadPost();
	    }
	  },
	  error: function(event) {
	    Ti.API.info("ERROR! " + JSON.stringify(event));
	    handleBadPost();
	  }
	});
});

Alloy.Globals.Facebook.addEventListener('login',function(e){
	if(e.success){
		var fbUser=JSON.parse(e.data);
		Ti.App.Properties.setString("logId",fbUser.id);
		Ti.App.Properties.setString("logName",fbUser.name);
		Ti.App.Properties.setString("logMedium","fb");
		var _token=Alloy.Globals.Facebook.getAccessToken();

		Alloy.Globals.Facebook.requestWithGraphPath('me/picture', {'redirect':'false','width':'230','hight':'230'}, 'GET', function(evt) {
		    if (evt.success) {
		        var fbResult=JSON.parse(evt.result);
		        var fbData=fbResult.data.url;
		        Ti.App.Properties.setString("profilePic",fbData);
		        $.index.close();
				Alloy.createController('profile').getView().open();
		    } else if (evt.error) {
		        Ti.API.info("Error: "+evt.error);
		    } else {
		        Ti.API.info('Unknown response');
		    }
		});
    } else if (e.cancelled){
    
    } else {
        Ti.API.debug('Failed authorization due to: ' + e.error);
    }
});

$.fbLogin.addEventListener('click',function(){
	Ti.API.info("Clicked");
	Alloy.Globals.Facebook.initialize(1000);
	Alloy.Globals.Facebook.authorize();
});

$.index.open();